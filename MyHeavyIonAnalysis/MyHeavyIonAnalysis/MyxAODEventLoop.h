#ifndef MyHeavyIonAnalysis_MyxAODEventLoop_H
#define MyHeavyIonAnalysis_MyxAODEventLoop_H

#include <EventLoop/Algorithm.h>
#include <TTree.h>
#include <TH1D.h>
#include <vector>
#include <string>
#include "AsgTools/AnaToolHandle.h"
#include "MuonMomentumCorrections/IMuonCalibrationAndSmearingTool.h"


//#include "GoodRunsLists/GoodRunsListSelectionTool.h"
//#include "TrigConfxAOD/xAODConfigTool.h"
//#include "TrigDecisionTool/TrigDecisionTool.h"
//#include "TrigMuonMatching/TrigMuonMatching.h"
//#include "MuonSelectorTools/MuonSelectionTool.h"
//#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
//#include "TriggerMatchingTool/MatchingTool.h"
//#include "TrigMuonMatching/TrigMuonMatching.h"
class MyxAODEventLoop : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
   std::string outputName;
   TTree *tree; //!
   TH1 *h_muonPt; //!
  //for tree
      //int  m_RunNumber; 						//!
      int  EventNumber; 					//!
      int  m_eventCounter;          //!
      //int  m_LumiBlock; 						//!
      //int  m_mcChannelNumber; 				//!
      //double  m_averageIntPerXing;            //!
      //double  m_actualIntPerXing;            //!

      // Muons
      //std::vector<float> *m_Muon_pt=0;
      // MuonCalibrationAndSmearing
      asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingToolHandle; //!


  // this is a standard constructor
  MyxAODEventLoop ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();
  //void clearVector();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODEventLoop, 1);
};

#endif
