#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "PathResolver/PathResolver.h"

#include "MyHeavyIonAnalysis/MyxAODEventLoop.h"
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //std::string inputFilePath = PathResolverFindCalibDirectory("$ALRB_TutorialData/p2622/");
  //SH::ScanDir().filePattern("DAOD_SUSY1.08377960._000012.pool.root.1").scan(sh,inputFilePath);
  std::string inputFilePath = PathResolverFindCalibDirectory("/mnt/Lustre/cgrp/atlas_hi/jpham/mc15_pPb8TeV/mc15_pPb8TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e6431_d1461_r10136_r9647/");
  SH::ScanDir().filePattern("AOD.13224498._000001.pool.root.1").scan(sh,inputFilePath);


  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setDouble (EL::Job::optMaxEvents, 500);

  // Add our analysis to the job:
  MyxAODEventLoop* alg = new MyxAODEventLoop();

  // define an output and an ntuple associated to that output
  EL::OutputStream output  ("myOutput");
  job.outputAdd (output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
  job.algsAdd (ntuple);
  job.algsAdd( alg );
  alg->outputName = "myOutput"; // give the name of the output to our algorithm

  // Run the job using the local/direct driver:
  EL::DirectDriver driver;
  driver.submit( job, submitDir );

  return 0;

  // Fetch and plot our histogram
  /*SH::SampleHandler sh_hist;
  sh_hist.load (submitDir + "/hist");
  TH1 *hist = (TH1*) sh_hist.get ("AOD.13224498._000001.pool.root.1")->readHist ("h_muonPt");
   hist->Draw ();*/
}
