#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyHeavyIonAnalysis/MyxAODEventLoop.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <TSystem.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TNtuple.h>
#include <TLorentzVector.h>
#include <vector>
#include <cmath>
#include <stdio.h>
// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
// ASG status code check
#include <AsgTools/MessageCheck.h>
// EDM includes:
#include "xAODEventInfo/EventInfo.h"
//Physics includes
#include "xAODMuon/MuonContainer.h"
#include "PATInterfaces/CorrectionCode.h" // to check the return correction code status of tools
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODEventLoop)



MyxAODEventLoop :: MyxAODEventLoop ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode MyxAODEventLoop :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD ();
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  ANA_CHECK(xAOD::Init());
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODEventLoop :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  h_muonPt = new TH1F("h_muonPt", "h_muonPt", 100, 0, 500); // muon pt [GeV]
  wk()->addOutput (h_muonPt);

  TFile *outputFile = wk()->getOutputFile (outputName);
  tree = new TTree ("HeavyIon_Nominal", "HeavyIon_Nominal");
  tree->SetDirectory (outputFile);
  tree->Branch("EventNumber", &EventNumber);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODEventLoop :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODEventLoop :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODEventLoop :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  xAOD::TEvent* event = wk()->xaodEvent();
  // count number of events
  m_eventCounter = 0;
// initialize the  ana tool handle for the muon calibration and smearing tool
  m_muonCalibrationAndSmearingToolHandle.setTypeAndName("CP::MuonCalibrationAndSmearingTool/MuonCorrectionToolHandle");
  ANA_CHECK(m_muonCalibrationAndSmearingToolHandle.retrieve());

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODEventLoop :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  xAOD::TEvent* event = wk()->xaodEvent();
    // print every 100 events, so we know where we are:
  if( (m_eventCounter % 100) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;

  //----------------------------
  // Event information
  //---------------------------
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve( eventInfo, "EventInfo"));
  //Filling in the event info
  EventNumber = eventInfo->eventNumber();
  std::cout << "EventNumber = " << EventNumber << std::endl;
  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  bool isMC = false;
  // check if the event is MC
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
       isMC = true; // can do something with this later
     }
       std::cout << "is MC? " << isMC << std::endl;

 //------------
 // MUONS
 //------------
   // get muon container of interest
   const xAOD::MuonContainer* muons = 0;
   ANA_CHECK(event->retrieve( muons, "Muons" ));


   // loop over the muons in the container
   xAOD::MuonContainer::const_iterator muon_itr = muons->begin();
   xAOD::MuonContainer::const_iterator muon_end = muons->end();
   for( ; muon_itr != muon_end; ++muon_itr ) {
        Info("execute()", "  original muon pt = %.2f GeV", ((*muon_itr)->pt() * 0.001)); // just to print out something
   } // end for loop over muons

 // create a shallow copy of the muons container
 std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
 // iterate over our shallow copy
 xAOD::MuonContainer::iterator muonSC_itr = (muons_shallowCopy.first)->begin();
 xAOD::MuonContainer::iterator muonSC_end = (muons_shallowCopy.first)->end();
 for( ; muonSC_itr != muonSC_end; ++muonSC_itr ) {
    if(m_muonCalibrationAndSmearingToolHandle->applyCorrection(**muonSC_itr) == CP::CorrectionCode::Error){// apply correction and check return code
         // Can have CorrectionCode values of Ok, OutOfValidityRange, or Error. Here only checking for Error.
         // If OutOfValidityRange is returned no modification is made and the original muon values are taken.
         Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
    }
 Info("execute()", "  corrected muon pt = %.2f GeV", ((*muonSC_itr)->pt() * 0.001) );
 h_muonPt->Fill((*muonSC_itr)->pt()/1000. );
 } // end for loop over shallow copied muons
 delete muons_shallowCopy.first;
 delete muons_shallowCopy.second;


  tree->Fill();
  return EL::StatusCode::SUCCESS;
}

/*void MyxAODEventLoop :: clearVector ()
{
  m_Muon_pt->clear();
}*/

EL::StatusCode MyxAODEventLoop :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODEventLoop :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODEventLoop :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
